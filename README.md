# <h1 align="center">Flask Blog App</h1>

<!-- ABOUT THE PROJECT -->
## About The Project

Blog application to create simple blog.
Features:
1. User can register and login
2. user can forget the password if user lost their password
3. user can create, update, read and delete the blog post

### Built With
 
* [Python](https://www.python.org/)
* [Flask](https://flask.palletsprojects.com/en/1.1.x/)
 
### Installation
 
1. Download or clone the Repository to your device
2. Create Virtual Environment using `python3 -m venv <your environment name>`
3. Activate Virtual Environment for Mac and Linux user `source <virtual env path>/bin/activate`
4. Activate Virtual Environment for Windows user `venv\Scripts\activate`
5. type `pip install -r requirements.txt` (this will install required package for project)
6. type `python3 run.py`

### Project Snap

#### Home Page
![Screenshot_2020-12-24_at_10.11.47_AM](/uploads/92586708e81801e2eab13ea2e9cd7d47/Screenshot_2020-12-24_at_10.11.47_AM.png)

#### Login and Register page
![Screenshot_2020-12-24_at_10.14.52_AM](/uploads/f3be939668ea07f1671453fe835c77d8/Screenshot_2020-12-24_at_10.14.52_AM.png)

#### Account and New Post form
![Screenshot_2020-12-24_at_10.18.58_AM](/uploads/782d7fb3904a5b704bfccc365f78ac78/Screenshot_2020-12-24_at_10.18.58_AM.png)

#### Update and Delete view
![Screenshot_2020-12-24_at_10.25.28_AM](/uploads/b6c7de4833588fe99945d3bc9bad7d47/Screenshot_2020-12-24_at_10.25.28_AM.png)
