from flask import Blueprint, jsonify
from flask import render_template, url_for, flash, redirect, request, abort
from flask_blog import db, bcrypt
from flask_login import login_user, current_user, logout_user, login_required
from flask_blog.models import User, RevokedTokenModel
from flask_mail import Message
from flask_blog.users.forms import (
    RegistrationForm,
    LoginForm,
    UpdateAccountFrom,
    RequestResetForm,
    ResetPasswordForm,
)
from flask_blog.users.utils import save_picture, send_reset_email
from flask_restful import Resource, Api, reqparse, abort, fields, marshal_with
from flask_jwt_extended import JWTManager
from flask_jwt_extended import (
    create_access_token,
    jwt_required,
    jwt_refresh_token_required,
    get_jwt_identity,
    get_raw_jwt,
)
from flask_blog import jwt

users_api = Blueprint("users_api", __name__)
api = Api(users_api)


@users_api.route("/api/register", methods=["POST"])
def register():
    username = request.form["username"]
    email = request.form["email"]
    password = request.form["password"]
    confirm_password = request.form["confirm_password"]
    if username == "" or email == "" or password == "" or confirm_password == "":
        return jsonify(
            {"message": "Please fill all required field", "status": "failed"}
        )
    if User.query.filter_by(username=username).first() is not None:
        return jsonify(
            {"message": "User Already exist with given username", "status": "failed"}
        )  # existing user
    else:
        try:
            hased_password = bcrypt.generate_password_hash(password).decode("utf-8")
            user = User(username=username, email=email, password=hased_password)
            db.session.add(user)
            db.session.commit()
            return jsonify(
                {
                    "message": "Your account has been created! You are now able to login",
                    "status": "success",
                }
            )
        except:
            return (
                jsonify(
                    {
                        "message": "Something went wrong while Creating Account!",
                        "status": "Failed",
                    }
                ),
                500,
            )


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token["jti"]
    return RevokedTokenModel.is_jti_blacklisted(jti)


@users_api.route("/api/login", methods=["POST"])
def login():
    if current_user.is_authenticated:
        return jsonify({"message": "already login"})
    email = request.form["email"]
    password = request.form["password"]
    if email == "" or password == "":
        return jsonify(
            {"message": "Please fill all required field", "status": "failed"}
        )
    try:
        user = User.query.filter_by(email=email).first()
        if user and bcrypt.check_password_hash(user.password, password):
            access_token = create_access_token(identity=email)
            return (
                jsonify(
                    {
                        "message": "Login successful",
                        "status": "success",
                        "access_token": access_token,
                        "code":200
                    }
                ),
            )

        else:
            return jsonify(
                {
                    "message": "Login Unsuccessful. Please check email and password",
                    "status": "failed",
                }
            )

    except:
        return jsonify(
            {
                "message": "Soemthing went wrong while login into your account",
                "status": "failed",
            }
        )


@users_api.route("/api/logout")
@jwt_required
def logout():
    jti = get_raw_jwt()["jti"]
    try:
        revoked_token = RevokedTokenModel(jti=jti)
        revoked_token.add()
        return jsonify({"message": "Logout Successful"})
    except:
        return jsonify({"message": "Something went wrong"})


@users_api.route("/api/reset_password", methods=["POST"])
def reset_request():
    email = request.form["email"]
    if email == "":
        return jsonify(
            {"message": "Please enter your email address", "status": "failed"}
        )
    else:
        user = User.query.filter_by(email=email).first()
        send_reset_email(user)
        return jsonify(
            {
                "message": "An email has been sent with instructions to reset your password.",
                "status": "success",
            }
        )
    return jsonify(
        {
            "message": "Error occured while reseting password! Please Check your email",
            "status": "failed",
        }
    )


@users_api.route("/api/reset_password/<token>", methods=["POST"])
def reset_token(token):
    user = User.verify_reset_token(token)
    if user is None:
        return jsonify(
            {"message": "That is an invalid or expired token", "status": "failed"}
        )
    password = request.form["password"]
    confirm_password = request.form["confirm_password"]

    if password == "" or confirm_password == "":
        return jsonify(
            {"message": "Please fill all required field", "status": "failed"}
        )
    elif password != confirm_password:
        return jsonify(
            {
                "message": "Your password and confirm password doesn't match",
                "status": "failed",
            }
        )
    else:
        hashed_password = bcrypt.generate_password_hash(password).decode("utf-8")
        user.password = hashed_password
        db.session.commit()
        flash("Your password has been updated! You are now able to log in", "success")
        return jsonify(
            {
                "message": "Your password has been updated! You are now able to log in",
                "status": "success",
            }
        )
