from flask import Blueprint, jsonify, request
from flask_blog.models import User, Post
from flask_login import current_user
from flask import render_template, url_for, flash, redirect, request, abort
from flask_blog import db
from flask_blog.posts.forms import PostForm
from flask_restful import Resource, Api, reqparse, abort, fields, marshal_with
from flask_jwt_extended import jwt_required, get_jwt_identity

post_api = Blueprint("post_api", __name__)
api = Api(post_api)


@post_api.route("/api/post/new", methods=["POST"])
@jwt_required
def new_post():
    current_user = get_jwt_identity()
    title = request.form["title"]
    content = request.form["content"]
    if title == "" or content == "":
        return jsonify(
            {"message": "Please fill all required field", "status": "failed"}
        )
    else:
        post = Post(
            title=request.form["title"],
            content=request.form["content"],
            user_id=current_user,
        )
        db.session.add(post)
        db.session.commit()
        return jsonify({"message": "Your post has been created!", "status": "success"})
    return jsonify({"message": "Error while creating post", "status": "failed"})


@post_api.route("/api/post/<int:post_id>", methods=["GET"])
def post(post_id):
    try:
        post = Post.query.get_or_404(post_id)
    except:
        return jsonify(
            {"message": "The Post you are looking for is not exist", "status": "failed"}
        )
    return jsonify({"post": post})


@post_api.route("/api/post/<int:post_id>/update", methods=["POST"])
@jwt_required
def update_post(post_id):
    current_user = get_jwt_identity()
    try:
        post = Post.query.get_or_404(post_id)
    except:
        return jsonify(
            {"message": "The Post you are looking for is not exist", "status": "failed"}
        )
    title = request.form["title"]
    content = request.form["content"]
    if post.user_id != current_user:
        return jsonify(
            {
                "message": "You don't have permission to edit this post",
                "status": "failed",
            }
        )
    if title == "" or content == "":
        return jsonify({"message": "Please enter title or content", "status": "failed"})
    else:
        post.title = title
        post.content = content
        db.session.commit()
        return jsonify({"message": "Your post has been upadated!", "status": "success"})


@post_api.route("/api/post/<int:post_id>/delete", methods=["DELETE"])
@jwt_required
def delete_post(post_id):
    current_user = get_jwt_identity()
    try:
        post = Post.query.get_or_404(post_id)
    except:
        return jsonify(
            {
                "message": "The Post you are looking for is not exist",
                "status": "failed",
            }
        )

    if post.user_id != current_user:
        return jsonify(
            {
                "message": "You don't have permission to delete this post",
                "status": "failed",
            }
        )
    db.session.delete(post)
    db.session.commit()
    return jsonify({"message": "Your post has been deleted!", "status": "success"})
