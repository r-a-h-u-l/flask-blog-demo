from flask import Blueprint
from flask_blog.models import Post
from flask import render_template

main = Blueprint("main", __name__)


@main.route("/")
@main.route("/home")
def home():
    posts = Post.query.all()
    return render_template("home.html", posts=posts)


@main.route("/about")
def about():
    return render_template("about.html")