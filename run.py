from flask_blog import create_app
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_migrate import Migrate


app = create_app()
admin = Admin()
admin.init_app(app)

from flask_blog.models import User, Post
from flask_blog import db

migrate = Migrate(app, db)


admin.add_view(ModelView(User, db.session))
admin.add_view(ModelView(Post, db.session))

if __name__ == "__main__":
    app.run(debug=True)